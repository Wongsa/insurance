﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.Data.OleDb;


namespace ConnectDataBase
{
    public class Excel
    {

        public double Connect(string gender, int age, string company, int time)
        {
            int times = time / 12;
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\work\Prakun_AIA_MTL.xlsx;
                                        Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";
            DataSet show = new DataSet();
            DataTable data = new DataTable();
            
            switch (gender)
            {
                case "ชาย":
                    gender = "MALE";
                    break;
                case "หญิง":
                    gender = "FEMALE";
                    break;
            }

            string sql = "";
            switch (company)
            {
                case "AIA":
                    company = "Prakun_AIA";
                    sql = "SELECT " + gender + " from [" + company + "$] where AGE_1 <= " + age.ToString() + "  AND AGE_2 >= " + age.ToString();
                    break;
                case "MTL":
                    company = "Prakun_MTL";
                     sql = "SELECT " + gender + " from [" + company + "$] where AGE_1 <= " + age.ToString() + 
                        " AND AGE_2 >= " + age.ToString() + " AND PK_YEAR = " + times.ToString();
                    break;
            }
            
            OleDbCommand command = new OleDbCommand
                (
                sql, conn
                );

            conn.Open();
            OleDbDataAdapter adapter = new OleDbDataAdapter(command);

            adapter.Fill(show);
            adapter.Fill(data);

            if (data.Rows.Count <= 0)
            {
                return 0;
            }
            return (double)data.Rows[0][0];
        }
    }
}
