﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditCalculatorModel;

namespace CreditCalculateService
{

    public class CalculatorLoan
    {
        //เงินสดที่เหลือ
        public double CashOver(double cash, double downPayment)
        {
            return Math.Round(cash - downPayment, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อทั้งสิ้น
        public double TotalLease(double CashOver, double insurance)
        {
            return Math.Round(CashOver + insurance, 2, MidpointRounding.AwayFromZero);
        }

        //ดอกเบี้ยเช่าซื้อทั้งหมด
        public double TotalInterest(double totalLease, double interest, int numPeriods)
        {
            return Math.Round((totalLease * (interest / 100.0)) * (numPeriods / 12.0), 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อรวมดอก
        public double Rent(double totalLease, double totalInterest)
        {
            return Math.Round(totalLease + totalInterest, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อ/เดือน
        public double LeasePerMonth(double rent, double numPeriods)
        {
            return Math.Round(rent / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อส่วนลด
        public double LeaseDiscountPerMonth(double totalLease, double interestOnTime, int numPeriods)
        {
            double leaseDiscount = (totalLease * (interestOnTime / 100.0)) * (numPeriods / 12.0);
            double leaseDiscountPerMonth = (leaseDiscount + totalLease) / numPeriods;

            return Math.Round(leaseDiscountPerMonth, 2, MidpointRounding.AwayFromZero);
        }

        //ส่วนลด
        public double DiscountPerMonth(double leasePerMonth, double leaseDiscountPerMonth)
        {
            return Math.Round(leasePerMonth - leaseDiscountPerMonth, 2, MidpointRounding.AwayFromZero);
        }

        //<---------------------------------------เช่าซื้อ--------------------------------------------------->

        //ภาษี
        public double CalVat(double totalLease, double totalInterest)
        {
            return Math.Round((totalLease + totalInterest) * 0.07, 2, MidpointRounding.AwayFromZero);
        }

        //ภาษี/เดือน
        public double CalVatPerMonth(double calVat, double numPeriods)
        {
            return Math.Round(calVat / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ค่างวดต่อเดือน
        public double CalPeriodsMonth(double totalLease, double TotalInterest, double numPeriods)
        {
            return Math.Ceiling(((totalLease + TotalInterest) / numPeriods)*100)/100;
        }

        //ค่าเช่าซื้อส่วนลดVat
        public double CalDiscountVat(double totalLease, double interestOnTime, double numPeriods)
        {
            double leaseDiscount = (totalLease * (interestOnTime / 100.0)) * (numPeriods / 12.0);
            double Vat = (totalLease + leaseDiscount) * 0.07;
            double discountVat = (leaseDiscount + totalLease + Vat) / numPeriods;

            return Math.Round(discountVat, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อรวมดอกVat
        public double CalRentVat(double totalLease, double totalInterest, double calVat)
        {
            return Math.Round(totalLease + totalInterest + calVat, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อ/เดือนVat
        public double LeasePerMonthVat(double rentVat, double numPeriods)
        {
            return Math.Round(rentVat / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ส่วนลดVat
        public double CalDiscountPerMonthVat(double leasePerMonthVat, double discountVat)
        {
            return Math.Round(leasePerMonthVat - discountVat, 2, MidpointRounding.AwayFromZero);
        }

        //________________________________________________________________________________________________________________________________//

        //calLoan
        public OutputModel CalculateLoan(CreditModel creditInput)
        {
            OutputModel output = new OutputModel();

            //เงินสดที่เหลือ
            output.CashOver = CashOver(creditInput.Cash, creditInput.DownPayment);

            //ค่าเช่าซื้อทั้งสิ้น
            output.TotalLease = TotalLease(output.CashOver, output.Insurance);

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            output.TotalInterest = TotalInterest(output.TotalLease, creditInput.Interest, creditInput.NumPeriods);

            //ค่าเช่าซื้อรวมดอก
            output.Rent = Rent(output.TotalLease, output.TotalInterest);

            //ค่าเช่าซื้อ/เดือน
            output.LeasePerMonth = LeasePerMonth(output.Rent, creditInput.NumPeriods);

            //ค่าเช่าซื้อส่วนลด
            output.LeaseDiscountPerMonth = LeaseDiscountPerMonth(output.TotalLease, creditInput.InterestOnTime, creditInput.NumPeriods);

            //ส่วนลด
            output.DiscountPerMonth = DiscountPerMonth(output.LeasePerMonth, output.LeaseDiscountPerMonth);

            return output;
        }

        //calLeasing
        public OutputModel CalculateLeasing(CreditModel creditInput)
        {
            OutputModel output = new OutputModel();
            //เงินสดที่เหลือ
            output.CashOver = CashOver(creditInput.Cash, creditInput.DownPayment);

            //ค่าเช่าซื้อทั้งสิ้น
            output.TotalLease = TotalLease(output.CashOver, output.Insurance);

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            output.TotalInterest = TotalInterest(output.TotalLease, creditInput.Interest, creditInput.NumPeriods);

            //ภาษี
            output.Vat = CalVat(output.TotalLease, output.TotalInterest);

            //ภาษี/เดือน
            output.VatPerMonth = CalVatPerMonth(output.Vat, creditInput.NumPeriods);

            //ค่างวด/เดือน
            output.PeriodsMonth = CalPeriodsMonth(output.TotalLease, output.TotalInterest, creditInput.NumPeriods);

            //ค่าเช่าซื้อรวมดอกVat
            output.Rent = CalRentVat(output.CashOver, output.TotalInterest, output.Vat);

            //ค่าเช่าซื้อต่อเดือนVat
            output.LeasePerMonthVat = LeasePerMonthVat(output.Rent, creditInput.NumPeriods);

            //ค่าเช่าซื้อส่วนลด
            output.DiscountVat = CalDiscountVat(output.TotalLease, creditInput.InterestOnTime, creditInput.NumPeriods);

            //ส่วนลดVat
            output.DiscountPerMonthVat = CalDiscountPerMonthVat(output.LeasePerMonthVat, output.DiscountVat);

            return output;
        }
        //บวกประกัน

        public OutputModel CalculateLoan2(CreditModel creditInput, OutputModel output)
        {

            //เงินสดที่เหลือ
            output.CashOver = CashOver(creditInput.Cash, creditInput.DownPayment);

            //ค่าเช่าซื้อทั้งสิ้น
            output.TotalLease = TotalLease(output.TotalLease, output.Insurance);

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            output.TotalInterest = TotalInterest(output.TotalLease, creditInput.Interest, creditInput.NumPeriods);

            //ค่าเช่าซื้อรวมดอก
            output.Rent = Rent(output.TotalLease, output.TotalInterest);

            //ค่าเช่าซื้อ/เดือน
            output.LeasePerMonth = LeasePerMonth(output.Rent, creditInput.NumPeriods);

            //ค่าเช่าซื้อส่วนลด
            output.LeaseDiscountPerMonth = LeaseDiscountPerMonth(output.TotalLease, creditInput.InterestOnTime, creditInput.NumPeriods);

            //ส่วนลด
            output.DiscountPerMonth = DiscountPerMonth(output.LeasePerMonth, output.LeaseDiscountPerMonth);

            return output;
        }

        //calLeasing
        public OutputModel CalculateLeasing2(CreditModel creditInput, OutputModel output)
        {

            //เงินสดที่เหลือ
            output.CashOver = CashOver(creditInput.Cash, creditInput.DownPayment);

            //ค่าเช่าซื้อทั้งสิ้น
            output.TotalLease = TotalLease(output.TotalLease, output.Insurance);

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            output.TotalInterest = TotalInterest(output.TotalLease, creditInput.Interest, creditInput.NumPeriods);

            //ภาษี
            output.Vat = CalVat(output.TotalLease, output.TotalInterest);

            //ภาษี/เดือน
            output.VatPerMonth = CalVatPerMonth(output.Vat, creditInput.NumPeriods);

            //ค่างวด/เดือน
            output.PeriodsMonth = CalPeriodsMonth(output.TotalLease, output.TotalInterest, creditInput.NumPeriods);

            //ค่าเช่าซื้อรวมดอกVat
            output.Rent= CalRentVat(output.TotalLease, output.TotalInterest, output.Vat);

            //ค่าเช่าซื้อต่อเดือนVat
            output.LeasePerMonthVat = LeasePerMonthVat(output.Rent, creditInput.NumPeriods);

            //ค่าเช่าซื้อส่วนลด
            output.DiscountVat = CalDiscountVat(output.TotalLease, creditInput.InterestOnTime, creditInput.NumPeriods);

            //ส่วนลดVat
            output.DiscountPerMonthVat = CalDiscountPerMonthVat(output.LeasePerMonthVat, output.DiscountVat);

            return output;
        }
    }
}
