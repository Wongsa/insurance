﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditCalculatorModel;
using ConnectDataBase;

namespace CreditCalculateService
{

    public class InsuranceCalculate
    {
        //InsuranceModel inputInsurance = new InsuranceModel();
        //OutputModel outputInsurance = new OutputModel();
        Excel excelDB = new Excel();

        public int m_Years;
        public int m_Months;
        public int m_Days;



        //วันเกิดนับตามปี
        public int CalAge(int birthdayYear)
        {
            return DateTime.Today.Year - birthdayYear;
        }

        //วันเกิดปีบริบูรณ์
        public int CalculateAge(int birthdayYear, int birthdayMonth, int birthdayDay)
        {
            int CurrentYears = DateTime.Today.Year;
            int CurrentMonths = DateTime.Today.Month;
            int CurrentDays = DateTime.Today.Day;

            if ((CurrentYears - birthdayYear) > 0 ||
                (((CurrentYears - birthdayYear) == 0) && ((birthdayMonth < CurrentMonths) ||
                  ((birthdayMonth == CurrentMonths) && (birthdayDay <= CurrentDays)))))
            {
                int DaysInBdayMonth = DateTime.DaysInMonth(birthdayYear, birthdayMonth);
                int DaysRemain = CurrentDays + (DaysInBdayMonth - birthdayDay);

                if (CurrentMonths > birthdayMonth)
                {
                    m_Years = CurrentYears - birthdayYear;
                    m_Months = CurrentMonths - (birthdayMonth + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    m_Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
                else if (CurrentMonths == birthdayMonth)
                {
                    if (CurrentDays >= birthdayDay)
                    {
                        m_Years = CurrentYears - birthdayYear;
                        m_Months = 0;
                        m_Days = CurrentDays - birthdayDay;
                    }
                    else
                    {
                        m_Years = (CurrentYears - 1) - birthdayYear;
                        m_Months = 11;
                        m_Days = DateTime.DaysInMonth(birthdayYear, birthdayMonth) - (birthdayDay - CurrentDays);
                    }
                }
                else
                {
                    m_Years = (CurrentYears - 1) - birthdayYear;
                    m_Months = CurrentMonths + (11 - birthdayMonth) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    m_Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
            }
            else
            {
                throw new ArgumentException("Birthday date must be earlier than current date");
            }

            return m_Years;
        }

        public double CalAIA(InsuranceModel inputInsurance, CreditModel inputCredit, OutputModel output)
        {
            return Math.Ceiling(output.Rent * (inputInsurance.InsuranceRate / 100) * (inputCredit.NumPeriods / 12));
        }

        public double CalMTL(InsuranceModel inputInsurance, CreditModel inputCredit, OutputModel output)
        {
            return Math.Ceiling(output.Rent * (inputInsurance.InsuranceRate / 100));
        }

        public int showCalAge(InsuranceModel inputInsurance)
        {
            int age = 0;
            switch (inputInsurance.Company)
            {
                case "AIA":
                    age = CalculateAge(inputInsurance.BirthdayYear, inputInsurance.BirthdayMonth, inputInsurance.BirthdayDay);
                    break;
                case "MTL":
                    age = CalAge(inputInsurance.BirthdayYear);
                    break;
            }
            return age;
        }
    }
}                            
