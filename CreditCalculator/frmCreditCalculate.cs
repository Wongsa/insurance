﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CreditCalculateService;
using CreditCalculatorModel;
using System.Data.OleDb;
using ConnectDataBase;


namespace CreditCalculator
{
    public partial class frmCreditCalculate : Form
    {
        public frmCreditCalculate()
        {
            InitializeComponent();
        }

        CalculatorLoan m_cal = new CalculatorLoan();
        InsuranceCalculate m_insuranceCal = new InsuranceCalculate();
        CreditModel m_inputModel = new CreditModel();
        OutputModel m_outputModel = new OutputModel();
        InsuranceModel m_insuranceInput = new InsuranceModel();
        Excel m_excelDB = new Excel();
        

        private void btnCalculate_Click(object sender, EventArgs e)
        {

            if (rdoLoan.Checked && rdoNo.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLoan(m_inputModel);
                showDataLoan();

            }
            else if (rdoLeasing.Checked && rdoNo.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLeasing(m_inputModel);
                showDataLeasing();

            }
            else if (rdoLoan.Checked && rdoAIA.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLoan(m_inputModel);
                m_outputModel.Insurance = m_insuranceCal.CalAIA(m_insuranceInput, m_inputModel, m_outputModel);
                m_outputModel = m_cal.CalculateLoan2(m_inputModel, m_outputModel);
                showDataLoan();

            }
            else if (rdoLeasing.Checked && rdoAIA.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLeasing(m_inputModel);
                m_outputModel.Insurance = m_insuranceCal.CalAIA(m_insuranceInput, m_inputModel, m_outputModel);
                m_outputModel = m_cal.CalculateLeasing2(m_inputModel, m_outputModel);
                showDataLeasing();
            }
            else if (rdoLoan.Checked && rdoMTL.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLoan(m_inputModel);
                m_outputModel.Insurance = m_insuranceCal.CalMTL(m_insuranceInput, m_inputModel, m_outputModel);
                m_outputModel = m_cal.CalculateLoan2(m_inputModel, m_outputModel);
                showDataLoan();
            }
            else if (rdoLeasing.Checked && rdoMTL.Checked && IsValidate())
            {
                m_outputModel = m_cal.CalculateLeasing(m_inputModel);
                m_outputModel.Insurance = m_insuranceCal.CalMTL(m_insuranceInput, m_inputModel, m_outputModel);
                m_outputModel = m_cal.CalculateLeasing2(m_inputModel, m_outputModel);
                showDataLeasing();
            }
            else
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ");
            }
        }

        private bool IsValidate()
        {
            if (txtCash.Text != "" && txtDownPayment.Text != "" && txtNumPeriods.Text != ""
                 && txtInterest.Text != "" && txtInterestOnTime.Text != "" && rdoNo.Checked)
            {
                setDataCredit();
                return true;
            }
            else if (cboGender.Text != "" && txtCash.Text != "" && txtDownPayment.Text != "" && txtNumPeriods.Text != ""
                 && txtInterest.Text != "" && txtInterestOnTime.Text != "" && txtCompany.Text != "" && ( rdoAIA.Checked || rdoMTL.Checked ))
            {
                setDataCredit();
                setDataCompany();
                return true;
            }
            return false;
        }

        private void setDataCompany()
        {
            m_insuranceInput.Company = txtCompany.Text;
            m_insuranceInput.Gender = cboGender.Text;
            m_insuranceInput.BirthdayDay = tmrBirthday.Value.Day;
            m_insuranceInput.BirthdayMonth = tmrBirthday.Value.Month;
            m_insuranceInput.BirthdayYear = tmrBirthday.Value.Year;
          

        }

        private void setDataCredit()
        {
            m_inputModel.Cash = double.Parse(txtCash.Text);
            m_inputModel.DownPayment = double.Parse(txtDownPayment.Text);
            m_inputModel.Interest = double.Parse(txtInterest.Text);
            m_inputModel.InterestOnTime = double.Parse(txtInterestOnTime.Text);
            m_inputModel.NumPeriods = int.Parse(txtNumPeriods.Text);
        }

        private void showDataLoan()
        {
            txtCashLeftovers.Text = m_outputModel.CashOver.ToString("#,###.00");
            txtTotalInterest.Text = m_outputModel.TotalInterest.ToString("#,###.00");
            txtRent.Text = m_outputModel.Rent.ToString("#,###.00");
            txtLeaseDiscountPerMonth.Text = m_outputModel.LeaseDiscountPerMonth.ToString("#,###.00");
            txtLeasePerMonth.Text = m_outputModel.LeasePerMonth.ToString("#,###.00");
            txtDiscountPerMonth.Text = m_outputModel.DiscountPerMonth.ToString("#,###.00");
            txtTotalLease.Text = m_outputModel.TotalLease.ToString("#,###.00");
            txtInsurance.Text = m_outputModel.Insurance.ToString("#,###.00");

        }

        private void showDataLeasing()
        {
            txtCashLeftovers.Text = m_outputModel.CashOver.ToString("#,###.00");
            txtTotalInterest.Text = m_outputModel.TotalInterest.ToString("#,###.00");
            txtTotalLease.Text = m_outputModel.TotalLease.ToString("#,###.00");
            txtVat.Text = m_outputModel.Vat.ToString("#,###.00");
            txtVatPerMonth.Text = m_outputModel.VatPerMonth.ToString("#,###.00");
            txtPeriodsMonth.Text = m_outputModel.PeriodsMonth.ToString("#,###.00");
            txtRent.Text = m_outputModel.Rent.ToString("#,###.00");
            txtLeaseDiscountPerMonth.Text = m_outputModel.DiscountVat.ToString("#,###.00");
            txtDiscountPerMonth.Text = m_outputModel.DiscountPerMonthVat.ToString("#,###.00");
            txtLeasePerMonth.Text = m_outputModel.LeasePerMonthVat.ToString("#,###.00");
            txtInsurance.Text = m_outputModel.Insurance.ToString("#,###.00");


        }

        private string TxtReplace()
        {
            string a = txtCash.Text.Replace(",", "");
            string b = a.Replace(".00", "");
            return b;
        }

        private void autoCalculation()
        {
            if (txtDownPayment.Text != "")
            {
                int cash = int.Parse(TxtReplace());
                double downPayment = double.Parse(txtDownPayment.Text);

                m_outputModel.CashOver = m_cal.CashOver(cash, downPayment);
                txtCashLeftovers.Text = m_outputModel.CashOver.ToString("#,###.00");
                txtTotalLease.Text = m_outputModel.CashOver.ToString("#,###.00");
            }
            else
            {
                txtCashLeftovers.Text = txtCash.Text;
                txtTotalLease.Text = txtCash.Text;
            }
        }

        private void txtDownPayment_KeyUp(object sender, KeyEventArgs e)
        {
            autoCalculation();
        }

        private void txtCash_KeyUp(object sender, KeyEventArgs e)
        {
            autoCalculation();
        }

        private void tmrBirthday_ValueChanged(object sender, EventArgs e)
        {
            if (DateTime.Today.Year > tmrBirthday.Value.Year)
            {
                SetAge();
            }
            else
            {
                MessageBox.Show("กรอกวันเกิดให้ถูกต้อง");
            }
            
        }

        private void SetAge()
        {
            setDataCompany();
            txtAge.Text = m_insuranceCal.showCalAge(m_insuranceInput).ToString();
        }

        public void CheckComma(object sender, EventArgs e)
        {
            if ((sender as TextBox).Text != "")
            {
                (sender as TextBox).Text = string.Format("{0:n}", double.Parse((sender as TextBox).Text));
                (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
            }
        }

        private void cboGender_SelectedValueChanged(object sender, EventArgs e)
        {
            if (IsValidate())
            {
                setDataCompany();
                ShowInsuranceRate();
            }
            else
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ");
            }
        }

        private void ShowInsuranceRate()
        {
            int age = int.Parse(txtAge.Text);
            m_insuranceInput.InsuranceRate = m_excelDB.Connect(m_insuranceInput.Gender, age, m_insuranceInput.Company, m_inputModel.NumPeriods);
            txtInsuranceRate.Text = m_insuranceInput.InsuranceRate.ToString();
        }

        private void SetCompany()
        {
            if (rdoNo.Checked)
            {
                txtCompany.Text = "ไม่ทำประกัน";
                tmrBirthday.Enabled = false;
            }
            else if (rdoAIA.Checked)
            {
                txtCompany.Text = "AIA";
                SetAge();
                ShowInsuranceRate();
                tmrBirthday.Enabled = true;
            }
            else
            {
                txtCompany.Text = "MTL";
                SetAge();
                ShowInsuranceRate();
                tmrBirthday.Enabled = true;
            }
        }

        private void rdoNo_Click(object sender, EventArgs e)
        {
            SetCompany();
        }

        private void rdoAIA_Click(object sender, EventArgs e)
        {
            SetCompany();
        }

        private void rdoMTL_Click(object sender, EventArgs e)
        {
            IsValidate();
            SetCompany();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtCash.Text = "";
            txtDownPayment.Text = "";
            txtCashLeftovers.Text = "";
            txtNumPeriods.Text = "";
            txtTotalLease.Text = "";
            txtTotalInterest.Text = "";
            txtRent.Text = "";
            txtLeasePerMonth.Text = "";
            txtLeaseDiscountPerMonth.Text = "";
            txtDiscountPerMonth.Text = "";
            txtVat.Text = "";
            txtPeriodsMonth.Text = "";
            txtVatPerMonth.Text = "";
        }

        private void txtCash_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }
        int m_dot = 0;
        double m_numDouble = 0;
        private void CheckTxt(object sender, KeyPressEventArgs e)
        {
           

            //เช็ค เลข,dot,-
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;


            // พิมได้ 13 ตัว
            if ((sender as TextBox).Text.Length == 13 && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') == -1 && e.KeyChar != '.') ||
                ((sender as TextBox).Text.Length == 13 + m_numDouble + 3 && ((sender as TextBox).Text.IndexOf('.') > -1 && e.KeyChar == '.')))
            {
                e.Handled = true;
            }

            //เช็ค.ตัวเดียว
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }

            //เช็ค.ให้ทศนิยม
            if ((sender as TextBox).Text.EndsWith("."))
            {
                m_dot = (sender as TextBox).Text.Length;
                m_numDouble = 0;
            }

            // ทศนิยม 2 ตำแหน่งให้หยุดพิม
            if (m_numDouble >= 2 && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != 8 && (sender as TextBox).SelectionStart >= m_dot)
            {
                e.Handled = true;
            }

            //Curser>dot&deleteให้ทศนิยม-- 
            if ((sender as TextBox).SelectionStart >= m_dot && e.KeyChar == 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_numDouble--;
            }

            //Curser>dot&!delete ใหทศนิยม++ = 2
            if ((sender as TextBox).SelectionStart >= m_dot && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_numDouble++;
                if (m_numDouble >= 2)
                {
                    m_numDouble = 2;
                }
            }

            //Curser<dot&!deleteให้dot++
            if ((sender as TextBox).SelectionStart < m_dot && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_dot++;
            }

            //Curser<dot&deleteให้dot-- =1
            if ((sender as TextBox).SelectionStart < m_dot && e.KeyChar == 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_dot--;
                if (m_dot <= 1)
                {
                    m_dot = 1;
                }
            }
        }

        private void txtDownPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }

        private void txtCash_Leave(object sender, EventArgs e)
        {
            CheckComma(sender, e);
        }

        private void txtDownPayment_Leave(object sender, EventArgs e)
        {
            CheckComma(sender, e);
        }
    }
}


