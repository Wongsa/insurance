﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCalculatorModel
{
    public class CreditModel
    {
        public double Cash { get; set; }
        public double DownPayment { get; set; }
        public int NumPeriods { get; set; }
        public double Interest { get; set; }
        public double InterestOnTime { get; set; }
    }
}
