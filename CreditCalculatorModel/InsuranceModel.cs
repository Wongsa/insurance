﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCalculatorModel
{
    public class InsuranceModel
    {
        public int BirthdayYear { get; set; }
        public int BirthdayMonth { get; set; }
        public int BirthdayDay { get; set; }
        public string Gender { get; set; }
        public string Company { get; set; }
        public double InsuranceRate { get; set; }
        
    }
}
