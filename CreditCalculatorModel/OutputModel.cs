﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCalculatorModel
{
    public class OutputModel
    {
        //ค่างวด
        public double CashOver { get; set; }
        public double TotalInterest { get; set; }
        public double TotalLease { get; set; }
        public double Rent { get; set; }
        public double LeasePerMonth { get; set; }
        public double LeaseDiscountPerMonth { get; set; }
        public double DiscountPerMonth { get; set; }
        public double Vat { get; set; }
        public double VatPerMonth { get; set; }
        public double PeriodsMonth { get; set; }
        public double DiscountVat { get; set; }
        public double LeasePerMonthVat { get; set; } 
        public double DiscountPerMonthVat { get; set; }

        //ประกัน
        public int Birthday { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public double InsuranceRate { get; set; }
        public double Insurance { get; set; }
    }
}
