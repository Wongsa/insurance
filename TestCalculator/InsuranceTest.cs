﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditCalculateService;
using CreditCalculatorModel;

namespace TestCalculator
{
    [TestClass]
    public class InsuranceTest
    {
        CreditModel m_inputCredit = new CreditModel();
        InsuranceModel m_insurance = new InsuranceModel();
        OutputModel m_output = new OutputModel();

        public void setAIA()
        {
            m_insurance.InsuranceRate = 1.23;
            m_inputCredit.NumPeriods = 24;
            m_output.Rent = 130000;
        }
        public void setMTL()
        {
            m_insurance.InsuranceRate = 2.947;
            m_inputCredit.NumPeriods = 24;
            m_output.Rent = 130000;
        }

        //อายุAIA
        [TestMethod]
        public void TestCalAge()
        {
            InsuranceCalculate insurance = new InsuranceCalculate();
            int age = insurance.CalAge(1995);
            Assert.AreEqual(22, age);
        }

        //อายุMTL
        [TestMethod]
        public void CalculateAge()
        {
            InsuranceCalculate insurance = new InsuranceCalculate();
            int age = insurance.CalculateAge(1995, 7, 12);
            Assert.AreEqual(22, age);
        }

        //อัตราเบี้ยประกันAIA
        [TestMethod]
        public void CalAIA()
        {
            InsuranceCalculate insurance = new InsuranceCalculate();
            setAIA();
            double AIA = insurance.CalAIA(m_insurance, m_inputCredit, m_output);
            Assert.AreEqual(3198.00, AIA);
        }

        //อัตราเบี้ยประกันMTL
        [TestMethod]
        public void CalMTL()
        {
            InsuranceCalculate insurance = new InsuranceCalculate();
            setMTL();
            double MTL = insurance.CalMTL(m_insurance, m_inputCredit, m_output);
            Assert.AreEqual(3832.00, MTL);
        }

        //อัตราเบี้ยประกันMTL
        [TestMethod]
        public void TotalLease()
        {
            CalculatorLoan Credit = new CalculatorLoan();
            double MTL = Credit.TotalLease(100000, 3832);
            Assert.AreEqual(103832.00, MTL);
        }


    }
}
