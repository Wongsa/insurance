﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditCalculateService;
namespace TestCalculator
{
    [TestClass]
    public class TestCal
    {

        //เงินสดส่วนที่เหลือ
        [TestMethod]
        public void TestCashOver()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calCashOver = cal.CashOver(150000, 50000);
            Assert.AreEqual(100000, calCashOver);
        }

        //ดอกเบี้ยเช่าซื้อทั้งหมด
        [TestMethod]
        public void TestTotalInterest()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double totalInterest = cal.TotalInterest(150000, 15, 24);
            Assert.AreEqual(45000.0, totalInterest);
        }

        //ค่าเช่าซื้อรวมดอกเบี้ย
        [TestMethod]
        public void TestRent()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calRent = cal.Rent(100000, 30000);
            Assert.AreEqual(130000, calRent);
        }

        //ค่าเช่าซื้อรวมดอกเบี้ยVat
        [TestMethod]
        public void TestRentVat()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calRent = cal.CalRentVat(100000, 30000, 9100);
            Assert.AreEqual(139100, calRent);
        }

        //ค่าเช่าซื้อ/เดือน
        [TestMethod]
        public void TestLeasePerMonth()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double leasePerMonth = cal.LeasePerMonth(130000, 24);
            Assert.AreEqual(5416.67, leasePerMonth);
        }

        //ค่าเช่าซื้อ/เดือนVat
        [TestMethod]
        public void TestLeasePerMonthVat()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double leasePerMonth = cal.LeasePerMonthVat(139100, 24);
            Assert.AreEqual(5795.83, leasePerMonth);
        }

        //ค่าเช่าซื้อส่วนลด
        [TestMethod]
        public void TestLeaseDiscountPerMonth()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double leaseDiscountPerMonth = cal.LeaseDiscountPerMonth(100000, 12, 24);
            Assert.AreEqual(5166.67, leaseDiscountPerMonth);
        }

        //ค่าเช่าซื้อส่วนลดvat
        [TestMethod]
        public void TestCalDiscountVat()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calDiscountVat = cal.CalDiscountVat(100000, 12, 24);
            Assert.AreEqual(5528.33, calDiscountVat);
        }

        //ภาษี
        [TestMethod]
        public void TestCalVat()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calVat = cal.CalVat(500000, 150000);
            Assert.AreEqual(45500, calVat);
        }

        //ภาษี/เดือน
        [TestMethod]
        public void TestCalVatPerMonth()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calVat = cal.CalVatPerMonth(9100, 24);
            Assert.AreEqual(379.17, calVat);
        }

        //ค่างวดต่อเดือน
        [TestMethod]
        public void TestCalPeriodsMonth()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double calPeriodsMonth = cal.CalPeriodsMonth(350000.00, 105000.00, 24);
            Assert.AreEqual(18958.34, calPeriodsMonth);
        }

        //ส่วนลด
        [TestMethod]
        public void TestDiscountPerMonth()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double discountPerMonth = cal.DiscountPerMonth(5416, 5166);
            Assert.AreEqual(250, discountPerMonth);
        }

        //ส่วนลดvat
        [TestMethod]
        public void TestDiscountVat()
        {
            CalculatorLoan cal = new CalculatorLoan();
            double discountVat = cal.DiscountPerMonth(20285.42, 19349.17);
            Assert.AreEqual(936.25, discountVat);
        }
    }
}

